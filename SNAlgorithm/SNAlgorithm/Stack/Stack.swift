//
//  Stack.swift
//  SNAlgorithm
//
//  Created by zsf on 16/8/9.
//  Copyright © 2016年 zsf. All rights reserved.
//

import UIKit

public struct Stack<T>
{
    private var array = [T]();
    
    public var isEmpty: Bool
    {
        return array.isEmpty;
    }
    
    public var count: Int
    {
        return array.count;
    }
    
    public mutating func push(element: T)
    {
        array.append(element);
    }
    
    public mutating func pop() -> T?
    {
        return array.popLast();
    }
    
    public func peek() -> T?
    {
        return array.last;
    }
}
